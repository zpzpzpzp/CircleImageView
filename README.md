# CircleImageView

## 简介
> CircleImageView是一个方便的将图片裁剪成圆形,也可以给图片设置边框

<img src="./image/circle.PNG"/>

## 下载安装
```shell
npm install @ohos/circleimageview --save
```
OpenHarmony npm环境配置等更多内容，请参考 [如何安装OpenHarmony npm包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_npm_usage.md) 。

## 使用说明

### CircleImageView引用及使用
```
import { CircleImageView } from '@ohos/circleimageview';

  aboutToAppear() {
    this.data.setImageURI('hugh.png').setDiameter(200).setBorderWidth(5)
      .setBorderColor(Color.White).setDisableCircularTransformation(true)
  }

  build() {
    Column() {
          List({ space: 10, initialIndex: 0 }) {
            ListItem() {
              Column() {
                CircleImageView({model:$data})
              }.align(Alignment.Center).width(200).height(200)
            }.editable(false).height('50%').width('100%')

            ListItem() {
              Column({ space: 5 }) {
                CircleImageView({model:$data})
              }.align(Alignment.Center)
            }.editable(false).height('50%').backgroundColor(0x000000).width('100%')
      }
    .width('100%').margin({ top: 5 })
    }
  }
```

## 接口说明
`@State data: CircleImageView.Model = new CircleImageView.Model()`
1. 设置图片路径
   `this.data.setImageURI()`
2. 设置设置裁剪大小
   `this.data.setDiameter()`
3. 设置边框宽度
   `this.data.setBorderWidth()`
3. 设置边框颜色
   `this.data.setBorderColor()`

## 兼容性
支持 OpenHarmony API version 8 及以上版本。

## 目录结构
````
|---- CircleImageView
|     |---- entry  # 示例代码文件夹
|     |---- circleimageview  # CircleImageView库文件夹
|     |     |      └─src
|     |     |      │---└─main  
|     |     |      |----   └─ets
|     |     |      │----      └──components  
|     |     |      │----             └──MainPage 
|     |     |      │----                  CircleImageView.ets #自定义图片裁剪
|     |---- index.ets  # 对外接口
|     |---- README.MD  # 安装使用方法                    
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/hihopeorg/CircleImageView/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/hihopeorg/CircleImageView/pulls) 。

## 开源协议
本项目基于 [Apache License 2.0](https://gitee.com/hihopeorg/CircleImageView/blob/master/LICENSE) ，请自由地享受和参与开源。


